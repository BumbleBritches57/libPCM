cmake_minimum_required(VERSION 3.0.2 FATAL_ERROR)

if (UNIX)
add_definitions(-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64)
endif

set(TrimSilence_BUILD "${CMAKE_CURRENT_SOURCE_DIR}/../BUILD/${ARCHITECTURE}/${BUILDTYPE}/TrimSilence")

add_executable(TrimSilence ${CMAKE_CURRENT_SOURCE_DIR}/TrimSilence.c)
TARGET_LINK_LIBRARIES(TrimSilence ${libPCM} ${FoundationIO} -lm)

if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_CLANG)
	set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS}" -flto)
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}" -std=c11 -fconst-strings -fshort-enums -vectorize-loops -funroll-loops -funsigned-char -Wno-c99-compat -fstrict-enums -ferror-limit=1024 -Weverything -fcolor-diagnostics)

	set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -g -O1")
	set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -Os")
else (CMAKE_COMPILER_IS_MSVC)
	set()
endif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_CLANG)
