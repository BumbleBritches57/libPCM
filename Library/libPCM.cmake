cmake_minimum_required(VERSION 3.0.2 FATAL_ERROR)

if (UNIX)
add_definitions(-D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64)
endif

set(libPCM_BUILD "${CMAKE_CURRENT_SOURCE_DIR}/../BUILD/${ARCHITECTURE}/${BUILDTYPE}/libPCM")
set(ARCHIVE_OUTPUT_DIRECTORY "${libPCM_BUILD}")

set(libPCM_Dir "${CMAKE_CURRENT_SOURCE_DIR}")

set(libPCM_Headers 
	"${libPCM_Dir}/include/libPCM.h"
	"${libPCM_Dir}/include/Private/libPCMTypes.h"
	"${libPCM_Dir}/include/Private/Audio/AIFCommon.h"
	"${libPCM_Dir}/include/Private/Audio/W64Common.h"
	"${libPCM_Dir}/include/Private/Audio/WAVCommon.h"
	"${libPCM_Dir}/include/Private/Image/BMPCommon.h"
	"${libPCM_Dir}/include/Private/Image/PXMCommon.h"
)

set(libPCM_Sources
	"${libPCM_Dir}/src/libPCM.c"
	"${libPCM_Dir}/src/Audio/AIFDecoder.c"
	"${libPCM_Dir}/src/Audio/AIFEncoder.c"
	"${libPCM_Dir}/src/Audio/W64Decoder.c"
	"${libPCM_Dir}/src/Audio/W64Encoder.c"
	"${libPCM_Dir}/src/Audio/WAVDecoder.c"
	"${libPCM_Dir}/src/Audio/WAVEncoder.c"
	"${libPCM_Dir}/src/Image/BMPDecoder.c"
	"${libPCM_Dir}/src/Image/BMPEncoder.c"
	"${libPCM_Dir}/src/Image/PXMDecoder.c"
	"${libPCM_Dir}/src/Image/PXMEncoder.c"
)

add_library(libPCM ${libPCM_Sources} ${libPCM_Headers})
add_library(libFoundationIO STATIC IMPORTED)
set_property(libFoundationIO IMPORTED_LOCATION "../Dependencies/FoundationIO/libFoundationIO/libFoundationIO.cmake")

TARGET_LINK_LIBRARIES(libPCM ${libFoundationIO})

if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_CLANG)
	set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS}" -flto)
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}" -std=c11 -ferror-limit=1024 -Weverything -fcolor-diagnostics -fconst-strings -fshort-enums -vectorize-loops -funroll-loops -funsigned-char -Wno-c99-compat -fstrict-enums)

	set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -g -O1")
	set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -Os")
endif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_CLANG

